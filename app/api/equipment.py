from flask import request, jsonify, url_for
from app.models import Equipment
from app.api import bp
from app import db
from app.api.errors import bad_request


@bp.route('/equipments/<int:serial>', methods=['GET'])
def get_equip(serial):
    return jsonify(Equipment.query.get_or_404(serial).to_dict())

@bp.route('/equipments', methods=['GET'])
def get_equips():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 5), 10)
    data = Equipment.to_collection_dict(Equipment.query, page, per_page, 'api.get_equips')
    return jsonify(data)

@bp.route('/equipments', methods=['POST'])
def create_equip():
    data = request.get_json() or {}
    if not data:
        return bad_request('Must contain something')
    equip = Equipment()
    equip.from_dict(data)
    db.session.add(equip)
    db.session.commit()
    response = jsonify(equip.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_equip', serial=equip.serial)
    return response

@bp.route('/equipments/<int:serial>', methods=['PUT'])
def update_equip(serial):
    equip = Equipment.query.get_or_404(serial)
    data = request.get_json() or {}
    if not data:
        return bad_request('Must contain something')
    equip.from_dict(data)
    db.session.commit()
    return jsonify(equip.to_dict())

@bp.route('/equipments/<int:serial>', methods=['DELETE'])
def delete_equip(serial):
    equip = Equipment.query.get_or_404(serial)
    db.session.delete(equip)
    db.session.commit()
    return jsonify(equip.to_dict())