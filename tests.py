#! python
import unittest
import json
from datetime import datetime
from app import create_app, db
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.equip = {
            "serial": 1234,
            "name": "David",
            "equip": "soldering iron",
            "phone": 728412
        }
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_build_create(self):
        res = self.client().post('/api/equipments', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        self.assertIn(self.equip['equip'], str(res.data))

    def test_get_all_equips(self):
        res = self.client().post('/api/equipments', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/api/equipments', headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)
        self.assertIn(self.equip['equip'], str(res.data))

    def test_get_equip_by_serial(self):
        rv = self.client().post('/api/equipments', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'","\""))
        result = self.client().get('/api/equipments/{}'.format(result_in_json['serial']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)
        self.assertIn(self.equip['equip'], str(result.data))

    def test_equip_can_be_edited(self):
        rv = self.client().post('/api/equipments', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        equip_old = self.equip['equip']
        self.equip['equip'] = 'airbrush'

        res = self.client().put('/api/equipments/{}'.format(result_in_json['serial']), data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)

        result = self.client().get('/api/equipments/{}'.format(result_in_json['serial']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.equip['equip'], str(result.data))
        self.assertNotIn(equip_old, str(result.data))

    def test_build_deletion(self):
        rv = self.client().post('/api/equipments', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        result = self.client().delete('/api/equipments/{}'.format(result_in_json['serial']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)

        result = self.client().get('/api/equipments/{}'.format(result_in_json['serial']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 404)

    def test_put_empty_object(self):
        rv = self.client().post('/api/equipments', data=json.dumps(self.equip), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        build_upd = {}

        res = self.client().put('/api/equipments/{}'.format(result_in_json['serial']), data=json.dumps(build_upd), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Must contain something', str(res.data))

        result = self.client().get('/api/equipments/{}'.format(result_in_json['serial']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.equip['equip'], str(result.data))

    def test_post_empty_object(self):
        res = self.client().post('/api/equipments', data=json.dumps({}), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Must contain something', str(res.data))


if __name__ == "__main__":
    unittest.main()